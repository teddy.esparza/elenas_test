# elenas_test

Repository created for the ELENAS'S Devops Test

In this Repository you will find the Infraestructure as Code implementation using Terraform.

**PIPELINE REPOSITORIES**

In these two Repositories you will find the code of the respective Docker Images, and also the CI/CD Pipelines to update the ECR:

- Client Side Render Frontend [repo here](https://gitlab.com/teddy.esparza/frontend_test)
- Backend service [repo here](https://gitlab.com/teddy.esparza/backend_test)


**Pre-requisites**

- One S3 Bucket to save the Terraform Status
- One AWS account, ACCESS_KEY_ID nad ACCESS_SECRET_KEY
- Two repositories where updload the Docker Images for frontend and Backend.

**Solution Overview:**

The both applications, **frontend and bakckend were dockerizaded**, the images will be uploaded to the respective container in AWS ECR. In a first version the images where saved in the GITLAB Repository.


**AWS Fargate** will be used to deploy the apps, one **cluster** on AWS ECS with **two services**, each one of this services associated with a t**ask definition** attached to the correspondind Docker image (in **ECR**).

The services are configured for **auto-scaling**.

The whole solution will be located in one specific **VPC**, we have one **LOAD BALANCER** to manage the traffic for the backend and frontend apps, the incomming traffic in the port **80** will be attached to the target group in the Frontend ECS service, the traffic in port **5000** will go to the Backend Service.  

**Six subnets** are created in the VPC, **3 for the load balancer** and **3 for the ECS Tasks** launched by the ECS Services. Each subnet is located in a **different AZ** in the specific **AWS Region**.

One **Internet Gateway** is used to make the **Load Balancer Internet-Faced**. The associated rules were created for allowing this (subnets for the load balance), in a former version, the subnets for the ECS task were also associated to these rules because they needed to reach the GITLAB Docker Repository.

**CI/CD PIPELINES**

GITLAB CI/CD is used for the deployment of the apps, each app (BK, FE) will be managed as and independent deploy, the pipeline will construct the Docker-image, then it will be pushed to the ECR REgistry and finally the associated Service will be updated forcing to e anew deployment.

**TODO**

Configure the ECS Services for using Blue/green Deployment with AWS CODEDEPLOY.
Create separate CLISTERS for each APP.
Associate the LB with a DNS entry (DNS 53 suggested)





