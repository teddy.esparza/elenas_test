resource "aws_appautoscaling_target" "elenas_target" {
  max_capacity = 5
  min_capacity = 1
  resource_id = "service/${var.ecs_cluster.name}/${var.ecs_service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace = "ecs"
}

resource "aws_appautoscaling_policy" "elenas_memory" {
  name               = "elenas-memory"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.elenas_target.resource_id
  scalable_dimension = aws_appautoscaling_target.elenas_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.elenas_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }

    target_value       = 80
  }
}

resource "aws_appautoscaling_policy" "elenas_cpu" {
  name = "elenas-cpu"
  policy_type = "TargetTrackingScaling"
  resource_id = aws_appautoscaling_target.elenas_target.resource_id
  scalable_dimension = aws_appautoscaling_target.elenas_target.scalable_dimension
  service_namespace = aws_appautoscaling_target.elenas_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value = 60
  }
}

# Backend

resource "aws_appautoscaling_target" "elenas_5000_target" {
  max_capacity = 5
  min_capacity = 1
  resource_id = "service/${var.ecs_cluster.name}/${var.ecs_5000_service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace = "ecs"
}

resource "aws_appautoscaling_policy" "elenas_5000_memory" {
  name               = "elenas-5000-memory"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.elenas_5000_target.resource_id
  scalable_dimension = aws_appautoscaling_target.elenas_5000_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.elenas_5000_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }

    target_value       = 80
  }
}

resource "aws_appautoscaling_policy" "elenas_5000_cpu" {
  name = "elenas-5000-cpu"
  policy_type = "TargetTrackingScaling"
  resource_id = aws_appautoscaling_target.elenas_5000_target.resource_id
  scalable_dimension = aws_appautoscaling_target.elenas_5000_target.scalable_dimension
  service_namespace = aws_appautoscaling_target.elenas_5000_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value = 60
  }
}

