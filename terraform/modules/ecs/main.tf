resource "aws_ecs_cluster" "elenas" {
  name = "elenas"
  capacity_providers = [
    "FARGATE"]
  setting {
    name = "containerInsights"
    value = "enabled"
  }

  tags = {
    Name = "elenas"
    Project = "elenas"
    Billing = "elenas"
  }
}

resource "aws_ecs_task_definition" "elenas" {
  family = "elenas"
  container_definitions = <<TASK_DEFINITION
  [
  {
    "portMappings": [
      {
        "hostPort": 80,
        "protocol": "tcp",
        "containerPort": 80
      }
    ],
    "cpu": 128,
    "environment": [
      {
        "name": "AUTHOR",
        "value": "Teddy"
      }
    ],
    "memory": 256,
    "image": "511420842629.dkr.ecr.us-east-2.amazonaws.com/frontendtest:latest",
    "essential": true,
    "name": "frontend",
    "networkMode": "awsvpc"
  }
]
TASK_DEFINITION

  network_mode = "awsvpc"
  requires_compatibilities = [
    "FARGATE"]
  memory = "512"
  cpu = "256"
  execution_role_arn = var.ecs_role.arn
  task_role_arn = var.ecs_role.arn

  tags = {
    Name = "elenas"
    Project = "elenas"
    Billing = "elenas"
  }
}

resource "aws_ecs_task_definition" "elenas_5000" {
  family = "elenas-5000"
  container_definitions = <<TASK_DEFINITION
  [
  {
    "portMappings": [
      {
        "hostPort": 5000,
        "protocol": "tcp",
        "containerPort": 5000
      }
    ],
    "cpu": 128,
    "environment": [
      {
        "name": "AUTHOR",
        "value": "Teddy"
      }
    ],
    "memory": 256,
    "image": "511420842629.dkr.ecr.us-east-2.amazonaws.com/backendtest:latest",
    "essential": true,
    "name": "backend",
    "networkMode": "awsvpc"    
  }
]
TASK_DEFINITION

  network_mode = "awsvpc"
  requires_compatibilities = [
    "FARGATE"]
  memory = "512"
  cpu = "256"
  execution_role_arn = var.ecs_role.arn
  task_role_arn = var.ecs_role.arn

  tags = {
    Name = "elenas-5000"
    Project = "elenas"
    Billing = "elenas"
  }
}

resource "aws_ecs_service" "elenas" {
  name = "elenas"
  cluster = aws_ecs_cluster.elenas.id
  task_definition = aws_ecs_task_definition.elenas.arn
  desired_count = 1
  launch_type = "FARGATE"
  platform_version = "1.4.0"

  lifecycle {
    ignore_changes = [
      desired_count]
  }

  network_configuration {
    subnets = [
      var.ecs_subnet_a.id,
      var.ecs_subnet_b.id,
      var.ecs_subnet_c.id]
    security_groups = [
      var.ecs_sg.id]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = var.ecs_target_group.arn
    container_name = "frontend"
    container_port = 80
  }
}

resource "aws_ecs_service" "elenas_5000" {
  name = "elenas-5000"
  cluster = aws_ecs_cluster.elenas.id
  task_definition = aws_ecs_task_definition.elenas_5000.arn
  desired_count = 1
  launch_type = "FARGATE"
  platform_version = "1.4.0"

  lifecycle {
    ignore_changes = [
      desired_count]
  }

  network_configuration {
    subnets = [
      var.ecs_subnet_a.id,
      var.ecs_subnet_b.id,
      var.ecs_subnet_c.id]
    security_groups = [
      var.ecs_sg.id]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = var.ecs_5000_target_group.arn
    container_name = "backend"
    container_port = 5000
  }
}