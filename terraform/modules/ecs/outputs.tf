output "ecs_cluster" {
  value = aws_ecs_cluster.elenas
}

output "ecs_service" {
  value = aws_ecs_service.elenas
}

output "ecs_5000_service" {
  value = aws_ecs_service.elenas_5000
}