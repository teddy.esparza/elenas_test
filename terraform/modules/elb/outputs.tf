output "elb" {
  value = aws_lb.elb_elenas
}

output "ecs_target_group" {
  value = aws_lb_target_group.ecs
}

output "ecs_5000_target_group" {
  value = aws_lb_target_group.ecs_5000
}