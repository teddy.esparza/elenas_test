# LOAD BALANCER

resource "aws_lb" "elb_elenas" {
  name               = "elenas"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [
    var.load_balancer_sg.id]
  subnets            = [
    var.load_balancer_subnet_a.id,
    var.load_balancer_subnet_b.id,
    var.load_balancer_subnet_c.id]

  tags = {
    Name = "elenas"
    Project = "elenas"
    Billing = "elenas"
  }
}

# LOAD BALANCER LISTENERS

resource "aws_lb_target_group" "ecs" {
  name     = "ecs"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc.id
  target_type = "ip"

  health_check {
    enabled             = true
    interval            = 300
    path                = "/"
    timeout             = 60
    matcher             = "200"
    healthy_threshold   = 5
    unhealthy_threshold = 5
  }

  tags = {
    Name = "elenas"
    Project = "elenas"
    Billing = "elenas"
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.elb_elenas.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ecs.arn
  }
}


resource "aws_lb_target_group" "ecs_5000" {
  name     = "ecs-5000"
  port     = 5000
  protocol = "HTTP"
  vpc_id   = var.vpc.id
  target_type = "ip"

  health_check {
    enabled             = true
    interval            = 300
    path                = "/"
    timeout             = 60
    matcher             = "200"
    healthy_threshold   = 5
    unhealthy_threshold = 5
  }

  tags = {
    Name = "elenas"
    Project = "elenas"
    Billing = "elenas"
  }
}

resource "aws_lb_listener" "http_5000" {
  load_balancer_arn = aws_lb.elb_elenas.arn
  port              = "5000"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ecs_5000.arn
  }
}
