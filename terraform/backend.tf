terraform {
  backend "s3" {
    bucket = "teddy-esparza-data-tf-state"
    region = "us-east-2"
    key = "terraform.tfstate"
  }
}